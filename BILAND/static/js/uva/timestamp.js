// FL-25-Mar-2013 Created
// FL-19-Dec-2013 Changed

var client_timestamp = "19-Dec-2013 11:50";		// in Toolbar About popup
var server_timestamp = "";

function getClientTimestamp()
{
	return client_timestamp;
}

function setServerTimestamp( timestamp )
{
	server_timestamp = timestamp;
}

function getServerTimestamp()
{
	return server_timestamp;
}

// [eof]
