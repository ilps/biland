# -*- coding: utf-8 -*-

"""
--------------------------------------------------------------------------------
Author:		Fons Laan, ILPS-ISLA, University of Amsterdam
Project		BiLand
Name:		urls.py
Version:	0.2
Goal:		services/urls definitions

FL-26-Mar-2013: Created
FL-09-Sep-2013: Changed
"""

from django.conf.urls.defaults import *

# some URLs do not end with '$' because their path varies and may be longer
urlpatterns = patterns( 'services.views',
	url( r'doc_count/$',       'doc_count' ),

	url( r'analytics/$',       'proxy' ),
	url( r'celery/$',          'proxy' ),
	url( r'export/$',          'proxy' ),

	url( r'xtas/key/$',        'proxy' ),
	url( r'xtas/manage/$',     'proxy' ),
	url( r'xtas/process/$',    'proxy' ),

	url( r'xtas/cloud/$',          'cloud' ),
	url( r'xtas/cloud_bytaskid/$', 'cloud_bytaskid' ),

	url( r'kb/',               'proxy' ),
	url( r'logger/$',          'proxy' ),
	url( r'retrieve/',         'proxy' ),
	url( r'search/$',          'proxy' ),
	url( r'scan/$',            'proxy' ),
	url( r'store/',            'proxy' ),
	url( r'cql2es/$',          'test_cql2es' ),
	url( r'translate/$',       'moses' ),
)

# [eof]
